import { Component, OnInit } from '@angular/core';
import { AuthService} from '../auth.service';
import {LoginService} from '../login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-banco',
  templateUrl: './banco.component.html',
  styleUrls: ['./banco.component.css']
})
export class BancoComponent implements OnInit {

  constructor(private as:AuthService, private ls:LoginService, private router : Router) { }

  ngOnInit() {
  }
  login(email,password){
  	let envio = {"email": email, "password":password};
  	this.ls.login(envio).subscribe(respuesta=>{
  		if(respuesta.estado == 1){ //lo que retorne el get
  			this.as.setUsername("alejo"); //el usuario retornado en el get
  			this.as.setIsUserLoggedIn(true); // a la hora de salir sencillamente eliminas las credenciales y lo pones en false
  			this.router.navigate(['home']) // en el caso de ser correcta la autenticación se dirige al componente de inicio, declarado en el app.module
		 				

  		}else{
  			//no se loggea muestra mensaje 
  			
  		} 
  	})
  	//Llamar el get y una vez recibida

  }
}
